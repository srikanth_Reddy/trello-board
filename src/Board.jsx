import React, { Component } from 'react'
import * as Api from './Api'
import Lists from './Lists'
import Star from './star.svg'

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = { board: {}, loading: true, err: false, length: 0 }
  }

  componentDidMount() {
    Api.GetBoard().then(
      res => this.setState({ ...this.state, board: res, loading: false, length: (res.name.length) * 12 }),
      err => this.setState({ ...this.state, loading: false, err: true })
    )
  }
  render() {

    let updateBoard = async (name) => {
      await Api.updateBoard(name)
      // .then(
      //   res => this.setState({ ...this.state, board: res, loading: false }),
      //   err => this.setState({ ...this.state, loading: false, err: true })
      // )
    }
    //styles
    let boardStyle = {
      backgroundColor: { ...this.state.board.prefs }.backgroundColor,
      minHeight: "100vh",
      padding: "1rem",
      backgroundImage: `url(${{ ...this.state.board.prefs }.backgroundImage})`
    }
    let header = {
      fontSize: "1.5rem",
      margin: "1rem",
      backgroundColor: { ...this.state.board.prefs }.backgroundColor,
      color: "black",
      maxWidth: `${this.state.length}px`,
      borderRadius: "5px"
    }
    let star = {
      width: "3rem",
      height: "2.5rem",
    }




    if (this.state.err) {
      return <div>Loading Failed</div>
    } else if (this.state.loading) {
      return <div>Loadind...</div>
    }
    else {
      return (
        <div className='board' style={boardStyle}><img style={star} alt="" src={Star} />
          <input
            className='header'
            style={header}
            defaultValue={this.state.board.name}
            onChange={(e) => {
              this.setState({ ...this.state, length: (e.target.value.length) * 12 })
              updateBoard(e.target.value)
            }}></input>
          <Lists />
        </div>
      )
    }

  }
}
