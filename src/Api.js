import axios from "axios";


let apiKey = '22382408d229176979776f23a95a3fd7';
let apiToken = 'fe9096979332a0b270ce10bbb7ae19fc226fbe176a33b98ed35afd739d244d87';
let id = '8ATkqJDD';
let idBoard = "633c52816498000045fec142";


export let GetBoard = () => {

   return axios.get(`https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let GetActions = () => {
   return axios.get(`https://api.trello.com/1/boards/${idBoard}/actions?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}



export let GetCard = (ID) => {
   return axios.get(`https://api.trello.com/1/boards/${id}/cards/${ID}?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let GetCards = () => {
   return axios.get(`https://api.trello.com/1/boards/${id}/cards?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let GetCheckLists = () => {
   return axios.get(`https://api.trello.com/1/boards/${id}/checklists?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let GetLists = () => {
   return axios.get(`https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}




// updation 
export let updateBoard = (name) => {
   return axios.put(`https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${apiToken}`, { name: name })
      .then(res => res.data)
}


export let createNewCard = (cardid, name) => {
   return axios.post(`https://api.trello.com/1/cards?idList=${cardid}&key=${apiKey}&token=${apiToken}`, { name: name })
      .then((res) => {
         return res.data
      })
}


export let createNewList = (boardId, listName) => {
   return axios.post(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let updateList = (listId, name) => {
   return axios.put(`https://api.trello.com/1/lists/${listId}?key=${apiKey}&token=${apiToken}`, { name: name })
      .then(res => res.data)
}

export let updateCard = (cardId, param, value) => {
   return axios.put(`https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`, { [param]: value })
      .then(res => res.data)
}

export let createCheckItem = (checklistId, itemname) => {

   return axios.post(`https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${itemname}&key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let createChecklist = (cardId, listName) => {
   return axios.post(`https://api.trello.com/1/checklists?idCard=${cardId}&key=${apiKey}&token=${apiToken}`, { name: listName })
      .then(res => res.data)
}

export let deleteChecklist = (checklistId) => {
   return axios.delete(`https://api.trello.com/1/checklists/${checklistId}?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let deleteCheckItem = (checkListId, idCheckItem) => {
   return axios.delete(`https://api.trello.com/1/checklists/${checkListId}/checkItems/${idCheckItem}?key=${apiKey}&token=${apiToken}`)
      .then(res => res.data)
}

export let updateCheckList = (listId, listName) => {
   return axios.put(`https://api.trello.com/1/checklists/${listId}?key=${apiKey}&token=${apiToken}`, { name: listName })
      .then(res => res.data)
}