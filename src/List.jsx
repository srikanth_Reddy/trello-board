import React, { Component } from 'react'
import * as Api from './Api'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Cards from './Cards'
import ListSvg from './list.svg'

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            loading: true,
            checkLists: [],
            addCard: false,
            err: false
        }

    }

    async componentDidMount() {
        await Api.GetCards().then(
            res => this.setState({ ...this.state, cards: res, loading: false }),
            err => this.setState({ ...this.state, loading: false, err: true })
        )
        await Api.GetCheckLists().then(
            res => this.setState({ ...this.state, checkLists: res })
        )
    }


    render() {
        let listHeader = {
            width: "11rem",
            border: "white"
        }
        let list = {
            display: "flex",
            rowGap: "1rem"
        }
        let listIcon = {
            width: "1.5rem",
            height: "1.5rem",
            margin: "0 .5rem 0 0"
        }

        let handleAddCard = () => {
            this.state.addCard ? this.setState({ ...this.state, addCard: false }) : this.setState({ ...this.state, addCard: true })
        }

        let createCard = async (e) => {
            e.preventDefault();
            handleAddCard()
            await Api.createNewCard(this.props.list.id, e.target.cardName.value);
            await Api.GetCards()
                .then((res) => this.setState({ ...this.state, cards: res }))
        }

        let UpdateCheckLists = async () => {
            await Api.GetCheckLists().then(
                res => this.setState({ ...this.state, checkLists: res })
            )
        }

        // filtering the checklist based on the cardId
        let filtering = (cardId) => {
            return this.state.checkLists.filter((x) => cardId === x.idCard ? true : false)
        }


        if (this.state.err) {
            return <div>Loading Failed</div>
        } else if (this.state.loading) {
            return <div>Loadind...</div>
        }
        else {
            return (
                <Card style={{ width: '15rem' }}>
                    <Card.Body>
                        <Card.Title><div style={list}>
                            <div>  <img style={listIcon} alt="" src={ListSvg} /></div>
                            <div>  <input
                                className='listHeader'
                                style={listHeader}
                                defaultValue={this.props.list.name}
                                onChange={(e) => Api.updateList(this.props.list.id, e.target.value)} /></div></div>

                        </Card.Title>
                        {
                            (this.state.cards).filter((card) => (this.props.list.id === card.idList) ? true : false)
                                .map((card) => <Cards
                                    cardid={card.id}
                                    key={card.id}
                                    card={card}
                                    UpdateCheckLists={UpdateCheckLists}
                                    checkLists={filtering(card.id)}

                                    name={this.props.list.name} />)
                        }

                        {!this.state.addCard && <div onClick={() => handleAddCard()}>+ AddCard</div>}

                        {this.state.addCard &&
                            <form onSubmit={(e) => createCard(e)}>
                                <input type="text" name="cardName" style={{ width: '13rem' }}></input>
                                <Button type="submit" variant="primary">+ AddCard</Button>
                                <Button onClick={() => handleAddCard()}>Close</Button>
                            </form>
                        }

                    </Card.Body>
                </Card>
            )
        }

    }
}






