import React, { Component } from 'react'
import * as Api from './Api'
import List from './List'
import Button from 'react-bootstrap/Button';


export default class Lists extends Component {
  constructor(props) {
    super(props);
    this.state = { lists: [], loading: true, addList: false, err: false }

  }

  async componentDidMount() {
    await Api.GetLists().then(
      res => this.setState({ ...this.state, lists: res, loading: false }),
      err => this.setState({ ...this.state, loading: false, err: true })
    )
  }


  render() {

    let listStyle = {
      display: "flex",
      gap: "1rem",
      overflowX: "scroll",
      height: "85vh"
    }

    let handleAddList = () => {
      this.state.addList ? this.setState({ ...this.state, addList: false }) : this.setState({ ...this.state, addList: true })
    }

    let createList = async (e) => {
      e.preventDefault();
      await Api.createNewList({ ...this.state.lists[0] }.idBoard, e.target.listName.value)
        .then(
          res => this.setState({ ...this.state, lists: res, loading: false }),
          err => this.setState({ ...this.state, loading: false, err: true })
        )
    }

    if (this.state.err) {
      return <div>Loading Failed</div>
    } else if (this.state.loading) {
      return <div>Loadind...</div>
    }
    else {
      return (
        <div style={listStyle}>
          {this.state.lists.map((list) => {
            return <div key={list.id}><List list={list} /></div>
          })}


          {!this.state.addList && <div
            onClick={() => handleAddList()}
            style={{
              padding: "1rem",
              backgroundColor: "white",
              height: "50px",
              minWidth: "13rem",
              borderRadius: "5px"
            }}>+ Add another list</div>}

          {this.state.addList &&
            <form onSubmit={(e) => createList(e)}>
              <div>  <input
                type="text"
                name="listName"
                placeholder='Enter the NewList name'
                style={{
                  width: '13rem', height: "50px",
                  borderRadius: "5px"
                }
                }
              ></input></div>
              <Button type="submit" variant="primary">+ AddList</Button>
              <Button onClick={() => handleAddList()}>Close</Button>
            </form>
          }
        </div>

      )
    }

  }
}
