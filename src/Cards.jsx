import React, { Component } from 'react'
import * as Api from './Api'
import './Cards.css'
import Button from 'react-bootstrap/Button';
import Svg from './checkList.svg'
import ClipBoard from './clipBoard.svg'

export default class Cards extends Component {
  constructor(props) {
    super(props);
    this.state = ({ cardDetails: {}, popup: false, addChecklist: false, addItem: false })
  }

  async componentDidMount() {
    await Api.GetCard(this.props.cardid).then(
      res => this.setState({ ...this.state, cardDetails: res })
    )
  }

  render() {

    // counthing the completed/total checklist items
    let data = this.props.checkLists.reduce((acc, cur) => {
      if (!acc[cur.idCard]) {

        acc[cur.idCard] = { completed: 0, total: 0 }
      }
      cur.checkItems.forEach((item) => {
        if (item.state === "complete") {
          acc[cur.idCard].completed += 1;
        }
        acc[cur.idCard].total += 1;

      })
      return acc;

    }, {})


    let handleAddChecklist = () => {
      this.state.addChecklist ? this.setState({ ...this.state, addChecklist: false }) : this.setState({ ...this.state, addChecklist: true })
    }

    let handleAddItem = () => {
      this.state.addItem ? this.setState({ ...this.state, addItem: false }) : this.setState({ ...this.state, addItem: true })
    }

    let handlePopup = () => {
      if (this.state.popup === true) {
        this.setState({ ...this.state, popup: false })
      } else {
        this.setState({ ...this.state, popup: true })
      }
    }

    let createCheckItem = async (itemName, checklistId) => {
      handleAddItem()

      await Api.createCheckItem(checklistId, itemName);
      this.props.UpdateCheckLists()
    }

    let createCheckList = async (cardId, listName) => {
      await Api.createChecklist(cardId, listName)
      handleAddChecklist()
      this.props.UpdateCheckLists()
    }

    let deleteChecklist = async (checklistId) => {
      await Api.deleteChecklist(checklistId);
      this.props.UpdateCheckLists()
    }

    let deleteCheckItem = async (listId, itemId) => {
      await Api.deleteCheckItem(listId, itemId)
      this.props.UpdateCheckLists()
    }

    return (
      <div>
        {/* //card on the list */}
        <div className='="card' style={{ border: "1px solid rgb(230,230,230)", marginBottom: ".5rem", padding: ".5rem" }} onClick={() => handlePopup()}>
          <div> {this.props.card.name} </div>
          {(data[this.props.card.id]) ?
            <div><img className='checkListSvg' alt="" src={Svg} /><span>{{ ...data[this.props.card.id] }.completed}/{{ ...data[this.props.card.id] }.total}</span> </div> : null}
        </div>

        {/* // card data on the popup */}

        {this.state.popup && <>
          <div className='overlay'>
            <div className='model'>

              <div className='card-details'>

                <input
                  className='header'
                  defaultValue={this.props.card.name}
                  onChange={(e) => Api.updateCard(this.props.card.id, "name", e.target.value)}></input>
                <div>in list {this.props.name}</div>

                <div><h5><img className='checkListSvg' alt="" src={ClipBoard} /> Description  </h5>

                  <textarea
                    defaultValue={this.state.cardDetails.desc}
                    onChange={(e) => Api.updateCard(this.props.card.id, "desc", e.target.value)}>
                  </textarea>
                </div>

                {/* //display checklist */}
                <div>{this.props.checkLists.map((Items) => {

                  return <div key={Items.id}>
                    <div className='checkLists'>
                      <div> <img className='checkListSvg' alt="" src={Svg} /><input
                        className='header'
                        defaultValue={Items.name}
                        onChange={(e) => Api.updateCheckList(Items.id, e.target.value)}></input></div>
                      <Button className='btn' onClick={() => deleteChecklist(Items.id)}
                      >X</Button>
                    </div>

                    {/* //display check item */}

                    <div className='checkList'>{Items.checkItems.map((item) => { 

                      return (item.state === "incomplete")
                        ?
                        <div className="checkitem" key={item.id}> <div> <input type="checkbox" value={item.name} />
                          <input
                            className='item'
                            defaultValue={item.name}
                            onChange={(e) => null}></input>
                        </div> <div><button onClick={() => deleteCheckItem(Items.id, item.id)}>x</button></div></div>
                        :
                        <div className="checkitem" key={item.id}> <div><input type="checkbox" value={item.name} checked readOnly={true} />
                          <input
                            className='item'
                            defaultValue={item.name}
                            onChange={(e) => null}></input>
                        </div> <div><button onClick={() => deleteCheckItem(Items.id, item.id)}>x</button></div></div>

                    })}</div><br />

                    {!this.state.addItem && <div
                      onClick={() => handleAddItem()}
                    > Add Item </div>}

                    {this.state.addItem &&
                      <form onSubmit={(e) => {
                        e.preventDefault()
                        createCheckItem(e.target.itemName.value, Items.id)
                      }}>
                        <div>  <textarea
                          type="text"
                          name="itemName"
                          placeholder='Enter the Item...'
                          style={{
                            width: '13rem',
                            borderRadius: "5px",
                          }
                          }
                        ></textarea></div>
                        <Button type="submit" variant="primary">+ Add</Button>
                        <Button onClick={() => handleAddItem()}>Close</Button>
                      </form>
                    }

                    <br /> </div>

                })}
                </div>

                {!this.state.addChecklist && <div
                  onClick={() => handleAddChecklist()}
                >+ Add Checklist</div>}

                {this.state.addChecklist &&
                  <form onSubmit={(e) => {
                    e.preventDefault()
                    createCheckList(this.props.card.id, e.target.listName.value)
                  }}>
                    <div>  <input
                      type="text"
                      name="listName"
                      placeholder='Enter the checklist name'
                      style={{
                        width: '13rem',
                        borderRadius: "5px",
                      }
                      }
                    ></input></div>
                    <Button type="submit" variant="primary">+ AddCheckList</Button>
                    <Button onClick={() => handleAddChecklist()}>Close</Button>
                  </form>
                }


              </div>
              <br />
              <button onClick={() => handlePopup()}>Close</button>
            </div></div></>}

      </div>


    )
  }

}

